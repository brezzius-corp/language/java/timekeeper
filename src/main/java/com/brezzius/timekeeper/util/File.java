package com.brezzius.timekeeper.util;

import com.aspose.cells.FileFormatType;
import com.aspose.cells.Workbook;
import com.aspose.cells.Worksheet;
import com.brezzius.timekeeper.model.Course;
import com.brezzius.timekeeper.model.Event;

import java.util.List;

public class File {
    public static void create(List<Event> events) {
        Workbook wb = new Workbook();

        Worksheet sheet = wb.getWorksheets().get(0);

        for(Event event : events) {
            for(Course course : event.courses()) {
                for(int i = 0, l = course.ranking().size(); i < l ; i++) {
                    sheet.getCells().get(i, 0).putValue(course.ranking().get(i).bib());
                    sheet.getCells().get(i, 1).putValue(course.ranking().get(i).place());
                    sheet.getCells().get(i, 2).putValue(course.ranking().get(i).firstname());
                    sheet.getCells().get(i, 3).putValue(course.ranking().get(i).lastname());
                    sheet.getCells().get(i, 4).putValue(course.ranking().get(i).gender());
                    sheet.getCells().get(i, 5).putValue(course.ranking().get(i).time());
                    sheet.getCells().get(i, 6).putValue(course.ranking().get(i).category());
                    sheet.getCells().get(i, 7).putValue(course.ranking().get(i).team());
                    sheet.getCells().get(i, 8).putValue(course.ranking().get(i).nationality());
                    sheet.getCells().get(i, 9).putValue(course.ranking().get(i).dnf());
                    sheet.getCells().get(i, 10).putValue(course.ranking().get(i).dsq());
                    sheet.getCells().get(i, 11).putValue(course.ranking().get(i).otl());
                }

                try {
                    wb.save(event.name().replaceAll(" ", "-").replaceAll("/", "-")
                            + "_"
                            + course.name().replaceAll(" ", "-").replaceAll("/", "-")
                            + ".xlsx", FileFormatType.EXCEL_2);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
