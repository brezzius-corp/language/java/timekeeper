package com.brezzius.timekeeper.model;

public record Ranking(Integer place, Integer bib, Integer time, Integer officialTime, String firstname, String lastname, String gender, String category, String team, String nationality, Boolean dnf, Boolean dsq, Boolean otl) { }
