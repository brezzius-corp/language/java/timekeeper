package com.brezzius.timekeeper.model;

import java.util.Date;
import java.util.List;

public record Course(Date date, String name, String link, List<Ranking> ranking) { }
