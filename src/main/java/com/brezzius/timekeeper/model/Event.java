package com.brezzius.timekeeper.model;

import java.util.Date;
import java.util.List;

public record Event(Date date, String name, String link, List<Course> courses) { }
