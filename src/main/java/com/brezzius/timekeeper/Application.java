package com.brezzius.timekeeper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.*;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.brezzius.timekeeper.model.Course;
import com.brezzius.timekeeper.model.Event;
import com.brezzius.timekeeper.model.Ranking;
import com.brezzius.timekeeper.util.File;

public class Application {

    private static final DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    public static void main(String[] args) {
        try {
            List<Event> events = new ArrayList<>();
            List<Course> courses = new ArrayList<>();
            List<Ranking> rankings = new ArrayList<>();

            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create("https://resultat.chrono-start.com/ranks/chrono-start"))
                    .GET()
                    .build();

            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

            Document doc = Jsoup.parse(response.body());

            Element dtEvents = doc.getElementById("dtEvents");
            Elements dtEvent, tr;

            Date eDate, cDate;
            String eName, eLink, cName, cLink;
            Integer place, bib, seconds, oseconds;
            Boolean dnf, dsq, otl;
            StringBuilder firstname;
            StringBuilder lastname;
            String gender;
            String time;
            String category;
            String team;
            String nationality;
            String[] names, times;

            Map<String, Integer> index = new HashMap<>();

            if(dtEvents != null) {
                dtEvent = dtEvents.getElementsByTag("table");

                for(int i = 1, l = dtEvent.size() ; i < l ; i++) {
                    tr = dtEvent.get(i).getElementsByTag("tr");

                    for(Element cc : tr) {
                        courses.clear();

                        eDate = sdf.parse(cc.getElementsByTag("td").get(0).text());
                        eName = cc.getElementsByTag("td").get(1).getElementsByTag("a").text();
                        eLink = cc.getElementsByTag("td").get(1).getElementsByTag("a").attr("href");

                        request = HttpRequest.newBuilder()
                                .uri(URI.create("https://resultat.chrono-start.com" + eLink))
                                .GET()
                                .build();

                        response = client.send(request, HttpResponse.BodyHandlers.ofString());

                        doc = Jsoup.parse(response.body());

                        Elements dtCourses = doc.getElementsByClass("panel panel-success border-apps");

                        for(int ii = 0, ll = dtCourses.size() ; ii < ll - 1 ; ii++) {
                            Elements labelDate = dtCourses.get(ii).getElementsByClass("text-muted zf-black");
                            Elements labelRace = dtCourses.get(ii).getElementsByClass("label-race");
                            Elements linkRank = dtCourses.get(ii).getElementsByClass("btn btn-success btn-sm");

                            for(Element rk : linkRank) {
                                rankings.clear();

                                /* Challenge entreprise non pris en compte */
                                if(rk.text().equals("Challenge Entreprise") || rk.text().equals("Challenge Enteprise"))
                                    continue;

                                /* Challenge collègue non pris en compte */
                                else if(rk.text().equals("Challenge Collegues"))
                                    continue;

                                /* Challenge corpo non pris en compte */
                                else if(rk.text().equals("Challenge Corpo"))
                                    continue;

                                cDate = sdf.parse(labelDate.text());
                                cName = labelRace.text();
                                cLink = rk.attr("href");

                                request = HttpRequest.newBuilder()
                                        .uri(URI.create("https://resultat.chrono-start.com" + cLink))
                                        .GET()
                                        .build();

                                response = client.send(request, HttpResponse.BodyHandlers.ofString());

                                doc = Jsoup.parse(response.body());

                                index.clear();

                                Elements hdRows = doc.getElementsByClass("well well-default").get(0).getElementsByTag("th");

                                for(int iii = 0, lll = hdRows.size() ; iii < lll ; iii++) {
                                    switch(iii) {
                                        case 0 -> {
                                            if (hdRows.get(iii).text().equals("Pl."))
                                                index.put("PLACE", 0);
                                        }
                                        case 1 -> {
                                            if (hdRows.get(iii).text().equals("Dos."))
                                                index.put("BIB", 1);
                                        }
                                        case 2 -> {
                                            if(hdRows.get(iii).text().equals("Nom-Prénom"))
                                                index.put("NAME", 2);
                                        }
                                        case 3 -> {
                                            if (hdRows.get(iii).text().equals("Temps"))
                                                index.put("TIME", 3);
                                        }
                                        case 4 -> {
                                            if (hdRows.get(iii).text().equals("Cat."))
                                                index.put("CATEGORY", 4);
                                            else if(hdRows.get(iii).text().equals("Temps réél"))
                                                index.put("OTIME", 4);
                                            else if(hdRows.get(iii).text().equals("Tps Course"))
                                                index.put("TIME", 4);
                                        }
                                        case 5 -> {
                                            if (hdRows.get(iii).text().equals("Club"))
                                                index.put("TEAM", 5);
                                            else if (hdRows.get(iii).text().equals("Cat."))
                                                index.put("CATEGORY", 5);
                                        }
                                        case 6 -> {
                                            if (hdRows.get(iii).text().equals("Club"))
                                                index.put("TEAM", 6);
                                        }
                                    }
                                }

                                if(index.containsKey("PLACE") && index.containsKey("BIB") && index.containsKey("NAME") && index.containsKey("TIME") && index.containsKey("CATEGORY") && index.containsKey("TEAM")) {
                                    Elements rankRows = doc.getElementsByClass("rank-row");

                                    for (Element rows : rankRows) {
                                        Elements rankRow = rows.getElementsByTag("td");

                                        switch (rankRow.get(index.get("PLACE")).getElementsByTag("span").get(0).text()) {
                                            case "DNF" -> {
                                                place = null;
                                                dnf = true;
                                                dsq = null;
                                                otl = null;
                                            }

                                            case "DSQ" -> {
                                                place = null;
                                                dnf = null;
                                                dsq = true;
                                                otl = null;
                                            }

                                            case "OTL" -> {
                                                place = null;
                                                dnf = null;
                                                dsq = null;
                                                otl = true;
                                            }

                                            default -> {
                                                place = Integer.parseInt(rankRow.get(index.get("PLACE")).getElementsByTag("span").get(0).text());
                                                dnf = null;
                                                dsq = null;
                                                otl = null;
                                            }
                                        }

                                        try {
                                            bib = Integer.parseInt(rankRow.get(index.get("BIB")).getElementsByTag("span").get(0).text());

                                            firstname = new StringBuilder();
                                            lastname = new StringBuilder();

                                            names = rankRow.get(index.get("NAME")).text().split(" ");

                                            if (rankRow.get(index.get("NAME")).getElementsByTag("img").size() > 0) {
                                                for (String name : names) {
                                                    if (name.chars().filter(Character::isUpperCase).count() == name.length())
                                                        lastname.append(name).append(" ");
                                                    else if (name.chars().filter(Character::isLowerCase).count() > 0)
                                                        firstname.append(name).append(" ");
                                                    else
                                                        lastname.append(name).append(" ");
                                                }

                                                if (firstname.length() > 0)
                                                    firstname = new StringBuilder(firstname.substring(0, firstname.length() - 1));

                                                if (lastname.length() > 0)
                                                    lastname = new StringBuilder(lastname.substring(0, lastname.length() - 1));

                                                gender = rankRow.get(index.get("NAME")).getElementsByTag("img").get(0).attr("alt");
                                            } else {
                                                firstname = new StringBuilder();
                                                lastname = new StringBuilder();
                                                gender = "";
                                            }

                                            time = rankRow.get(index.get("TIME")).getElementsByTag("div").get(0).text().replace("h", ":").replace("'", ":");
                                            times = time.split(":");

                                            if (times.length == 1 && !times[0].equals("Abandon") && !times[0].equals("Hors Délais") && !times[0].equals("Hors Délais") && !times[0].equals("Disqualifié"))
                                                seconds = Integer.parseInt(times[0]);
                                            else if (times.length == 2)
                                                seconds = Integer.parseInt(times[0]) * 60 + Integer.parseInt(times[1]);
                                            else if (times.length == 3)
                                                seconds = Integer.parseInt(times[0]) * 3600 + Integer.parseInt(times[1]) * 60 + Integer.parseInt(times[2]);
                                            else
                                                seconds = 0;

                                            if(index.containsKey("OTIME")) {
                                                time = rankRow.get(index.get("OTIME")).getElementsByTag("div").get(0).text().replace("h", ":").replace("'", ":");
                                                times = time.split(":");

                                                if (time.length() == 1)
                                                    oseconds = Integer.parseInt(times[0]);
                                                else if (time.length() == 2)
                                                    oseconds = Integer.parseInt(times[0]) * 60 + Integer.parseInt(times[1]);
                                                else if (time.length() == 3)
                                                    oseconds = Integer.parseInt(times[0]) * 3600 + Integer.parseInt(times[1]) * 60 + Integer.parseInt(times[2]);
                                                else
                                                    oseconds = 0;
                                            } else
                                                oseconds = 0;

                                            category = rankRow.get(index.get("CATEGORY")).getElementsByTag("div").get(0).text();

                                            if (rankRow.get(index.get("TEAM")).getElementsByTag("img").size() > 0) {
                                                team = rankRow.get(index.get("TEAM")).text();
                                                nationality = rankRow.get(index.get("TEAM")).getElementsByTag("img").get(0).attr("alt");
                                            } else {
                                                team = "";
                                                nationality = "";
                                            }

                                            rankings.add(new Ranking(place, bib, seconds, oseconds, firstname.toString(), lastname.toString(), gender, category, team, nationality, dnf, dsq, otl));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                } else {
                                    System.out.println(eName + " - " + cName + " => Non pris en charge");
                                }

                                courses.add(new Course(cDate, cName, cLink, rankings));
                            }
                        }

                        events.add(new Event(eDate, eName, eLink, courses));

                        File.create(events);
                    }
                }
            }

            //Debug.showCourses(events);

        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
